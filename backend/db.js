

const mysql = require('mysql2')

const pool = mysql.createPool({

  host: 'moviedb',
  user: 'root',
  password: 'root',
  database: 'MovieDb',
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
})

module.exports = pool
